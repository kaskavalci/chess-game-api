package http

import (
	"encoding/json"
	"net/http"

	"gitlab.com/kaskavalci/chess-game-api/chess"
)

func serveError(err error, w http.ResponseWriter) {
	chessErr, ok := err.(chess.Error)
	if !ok {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte(err.Error()))
		return
	}
	data, err := json.Marshal(chessErr)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	w.WriteHeader(http.StatusConflict)
	w.Write(data)
}
