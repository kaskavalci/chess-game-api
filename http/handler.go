package http

import (
	"fmt"
	"log"
	"net/http"
	"strconv"

	"gitlab.com/kaskavalci/chess-game-api/background"
	"gitlab.com/kaskavalci/chess-game-api/chess"

	"github.com/julienschmidt/httprouter"
	"gitlab.com/kaskavalci/chess-game-api/service"
)

const (
	GameHash = "X-GameHash"
)

// Config for HTTP Handler
type Config struct {
	URI        string
	Port       int
	Logger     *log.Logger
	Service    service.Service
	Background background.Process
	Quit       chan error
}

// Handler handles http connections
type handler struct {
	*Config
	router *httprouter.Router
}

type Handler interface {
	ListenAndServe()
}

// New creates a new HTTP Handler instace
func New(config *Config) Handler {
	if config == nil {
		panic("nil config")
	}

	h := handler{
		router: httprouter.New(),
		Config: config,
	}

	h.router.GET("/health", h.handleHealth)
	h.router.POST("/game", h.handleNewGame)
	h.router.GET("/board", h.handleBoard)
	h.router.GET("/status", h.handleStatus)
	h.router.POST("/move", h.handleMakeMove)
	h.router.POST("/background", h.handleRunBG)

	h.router.NotFound = http.FileServer(http.Dir("../static/"))

	http.Handle("/", h.router)

	return &h
}

// ListenAndServe - Blocks the current goroutine, opens an HTTP port and serves the web REST requests
func (s *handler) ListenAndServe() {
	s.Quit <- http.ListenAndServe(fmt.Sprintf("%s:%d", s.URI, s.Port), s.router)
}

func getHash(r *http.Request) (int, error) {
	hashStr := r.Header.Get(GameHash)
	if hashStr == "" {
		return 0, chess.ErrInvalidGame
	}
	hash, err := strconv.Atoi(hashStr)
	if err != nil {
		chessErr := chess.ErrInvalidGame
		chessErr.Message = fmt.Sprintf("invalid hash number: %v", err)
		return 0, chessErr
	}
	return hash, nil
}

func (s *handler) handleHealth(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	w.WriteHeader(http.StatusOK)
}
func (s *handler) handleRunBG(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	s.Background.Perform()
	w.WriteHeader(http.StatusOK)
}

func (s *handler) handleNewGame(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	hash, err := s.Service.New()
	if err != nil {
		serveError(err, w)
		return
	}
	w.Header().Add(GameHash, strconv.Itoa(hash))
	w.WriteHeader(http.StatusOK)
}

func (s *handler) handleStatus(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	hash, err := getHash(r)
	if err != nil {
		serveError(err, w)
		return
	}

	status, err := s.Service.Status(hash)
	if err != nil {
		serveError(err, w)
		return
	}

	w.Write([]byte(status))
}
func (s *handler) handleBoard(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	hash, err := getHash(r)
	if err != nil {
		serveError(err, w)
		return
	}

	board, err := s.Service.Board(hash)
	if err != nil {
		serveError(err, w)
		return
	}
	w.Header().Set("Content-Type", "text/plain; charset=utf-8")
	w.Write([]byte(board))
}

func (s *handler) handleMakeMove(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	move := r.FormValue("m")
	hash, err := getHash(r)
	if err != nil {
		serveError(err, w)
		return
	}
	result, err := s.Service.Move(hash, move)
	if err != nil {
		serveError(err, w)
		return
	}
	w.Write([]byte(result))
}
