package main

import (
	"log"
	"os"
	"strconv"

	"gitlab.com/kaskavalci/chess-game-api/background"
	"gitlab.com/kaskavalci/chess-game-api/http"
	"gitlab.com/kaskavalci/chess-game-api/service"
)

func main() {
	uri := os.Getenv("URI")
	portStr := os.Getenv("PORT")
	logger := log.New(os.Stdout, "chess-game", log.LstdFlags)
	port, err := strconv.Atoi(portStr)
	if err != nil {
		logger.Printf("port %s is not a valid integer. 80 will be used", portStr)
		port = 80
	}
	quit := make(chan error)

	service := service.New(&service.Config{
		Logger: logger,
	})

	bg := background.Process{
		Service: service,
	}

	handler := http.New(&http.Config{
		URI:        uri,
		Port:       port,
		Logger:     logger,
		Service:    service,
		Background: bg,
		Quit:       quit,
	})

	go handler.ListenAndServe()
	go bg.Run()

	err = <-quit
	logger.Fatalf("Error occurred: %v", err)
}
