package background

import (
	"time"

	"gitlab.com/kaskavalci/chess-game-api/service"
)

type Process struct {
	Service service.Service
}

func (p *Process) Perform() {
	p.Service.DeleteObsoleteGames()

}

func (p *Process) Run() {
	ticker := time.NewTicker(60 * time.Minute)

	for {
		<-ticker.C

		p.Perform()
	}

}
