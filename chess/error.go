package chess

import "fmt"

type ErrorCode string

type Error struct {
	Message string    `json:"message,omitempty"`
	Code    ErrorCode `json:"code,omitempty"`
}

func (err Error) String() string {
	return fmt.Sprintf("%s - %s", err.Code, err.Message)
}

func (err Error) Error() string {
	return err.String()
}
