package chess

var (
	ErrInvalidGame      = Error{Message: "Game is not Found", Code: InvalidGame}
	ErrCannotCreateGame = Error{Message: "Cannot initialize a new game", Code: NoNewGame}
)

const (
	InvalidGame = ErrorCode("InvalidGame")
	NoNewGame   = ErrorCode("NoNewGame")
)
