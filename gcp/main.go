package main

import (
	"log"
	"os"

	"gitlab.com/kaskavalci/chess-game-api/background"
	"gitlab.com/kaskavalci/chess-game-api/http"
	"gitlab.com/kaskavalci/chess-game-api/service"
	"google.golang.org/appengine"
)

func main() {
	uri := os.Getenv("URI")
	logger := log.New(os.Stdout, "chess-game", log.LstdFlags)
	quit := make(chan error)

	service := service.New(&service.Config{
		Logger: logger,
	})

	bg := background.Process{
		Service: service,
	}

	http.New(&http.Config{
		URI:        uri,
		Logger:     logger,
		Service:    service,
		Background: bg,
		Quit:       quit,
	})

	appengine.Main()
}
