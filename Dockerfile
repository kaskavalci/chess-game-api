# BUILD
FROM golang:1.11-alpine3.8

# install dependencies
RUN apk add --no-cache git musl-dev gcc

COPY . /go/src/gitlab.com/kaskavalci/chess-game-api
WORKDIR /go/src/gitlab.com/kaskavalci/chess-game-api

RUN go build -o chess-game-api

ENV PORT=80
EXPOSE 80
ENTRYPOINT ["./chess-game-api"]
