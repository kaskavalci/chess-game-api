package service

import (
	"log"
	"math/rand"
	"time"

	"gitlab.com/kaskavalci/chess-game-api/chess"
)

// Config for Chess game engine
type Config struct {
	Logger       *log.Logger
	TimeToDelete time.Duration
}

type service struct {
	*Config
	games map[int]*Game // games in hashmap
	rng   *rand.Rand
}

type Service interface {
	New() (int, error)
	Move(game int, moveStr string) (string, error)
	Board(game int) (string, error)
	Status(game int) (string, error)
	DeleteObsoleteGames() error
}

// New creates a new Service instace
func New(config *Config) Service {
	if config == nil {
		panic("nil config")
	}
	config.TimeToDelete = 2 * time.Second
	if config.TimeToDelete == 0 {
		config.TimeToDelete = 2 * time.Second
	}

	s := service{
		Config: config,
		games:  make(map[int]*Game),
		rng:    rand.New(rand.NewSource(time.Now().Unix())),
	}

	return &s
}

func (s *service) New() (int, error) {
	hash := s.rng.Int()
	var found bool
	for i := 0; i < 10; i++ {
		_, found = s.games[hash]
		if found {
			continue
		}
		hash = s.rng.Int()
	}
	if found {
		return 0, chess.ErrCannotCreateGame
	}

	s.games[hash] = NewGame()

	return hash, nil
}

func (s *service) Move(game int, moveStr string) (string, error) {
	g, ok := s.games[game]
	if !ok {
		return "", chess.ErrInvalidGame
	}
	move, err := g.Game.Position().ParseMove(moveStr)
	if err != nil {
		return "", err
	}

	status, err := g.Game.MakeMove(move)
	if err != nil {
		return "", err
	}

	g.LastMove = time.Now().UTC()

	return status.String(), nil
}

func (s *service) Board(game int) (string, error) {
	g, ok := s.games[game]
	if !ok {
		return "", chess.ErrInvalidGame
	}
	return g.Game.String(), nil
}

func (s *service) Status(game int) (string, error) {
	g, ok := s.games[game]
	if !ok {
		return "", chess.ErrInvalidGame
	}
	return g.Game.Status().String(), nil
}

func (s *service) DeleteObsoleteGames() error {
	for hash, g := range s.games {
		if g.LastMove.Add(s.TimeToDelete).Before(time.Now().UTC()) {
			delete(s.games, hash)
		}
	}

	return nil
}
