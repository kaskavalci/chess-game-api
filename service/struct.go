package service

import (
	"time"

	"github.com/andrewbackes/chess/game"
)

type Game struct {
	Game     *game.Game
	LastMove time.Time
}

func NewGame() *Game {
	return &Game{
		Game:     game.New(),
		LastMove: time.Now().UTC(),
	}
}
