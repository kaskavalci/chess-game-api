# Chess Game API

This API / UI uses hard work of [andrewbackes](https://github.com/andrewbackes/chess)'s chess game and wrap it with a simple user interface. Game supports multiple players playing at the same time but does not persist game data.

Application is deployed to Google App Engine and available from [https://chess-board-225417.appspot.com/](https://chess-board-225417.appspot.com/)

## Endpoints

### Create a new game `POST /game`

Game session ID is provided as header in new game endpoint. Expected header key is `X-Gamehash`.

```http
> POST /game HTTP/1.1
> Host: chess-board-225417.appspot.com
> Accept: */*
> Content-Length: 0
< HTTP/1.1 200 OK
< X-Gamehash: 7725442681239953829
```

### Make Move `POST /move?m=...`

Players play in turns. Each time there is a move, game status is returned.

```http
> POST /move?m=e2e4 HTTP/1.1
> Host: chess-board-225417.appspot.com
> X-Gamehash: 654941649018891874
> Accept: */*
> Content-Length: 0
< HTTP/1.1 200 OK
< Content-Type: text/html; charset=utf-8
In Progress
```

### Get Board Status `GET /board`

Board is provided as `text/plain`.

```http
> GET /board HTTP/1.1
> Host: chess-board-225417.appspot.com
> X-Gamehash: 7725442681239953829
> Accept: */*
< HTTP/1.1 200 OK
< Content-Type: text/plain; charset=utf-8
   +---+---+---+---+---+---+---+---+
 8 | r | n | b | q | k | b | n | r |   Active Color:    White
   +---+---+---+---+---+---+---+---+
 7 | p | p | p | p | p | p | p | p |
   +---+---+---+---+---+---+---+---+
 6 |   |   |   |   |   |   |   |   |   En Passant:      None
   +---+---+---+---+---+---+---+---+
 5 |   |   |   |   |   |   |   |   |   Castling Rights: KQkq
   +---+---+---+---+---+---+---+---+
 4 |   |   |   |   |   |   |   |   |   50 Move Rule:    0
   +---+---+---+---+---+---+---+---+
 3 |   |   |   |   |   |   |   |   |
   +---+---+---+---+---+---+---+---+
 2 | P | P | P | P | P | P | P | P |   White's Clock:   0s (0 moves)
   +---+---+---+---+---+---+---+---+
 1 | R | N | B | Q | K | B | N | R |   Black's Clock:   0s (0 moves)
   +---+---+---+---+---+---+---+---+
     A   B   C   D   E   F   G   H
```

### Get Game Status `GET /status`

```http
> GET /board HTTP/1.1
> Host: chess-board-225417.appspot.com
> X-Gamehash: 7725442681239953829
> Accept: */*
< HTTP/1.1 200 OK
< Content-Type: text/html; charset=utf-8
In Progress
```

## Known Bugs

Since Google App Engine manages application runtime it is common to have it restarted or another instance of the app is provided from load balancer. Since sessions are not persisted, game can be broken during the play. This problem does not exist with local installations.
