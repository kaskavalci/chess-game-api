.PHONY: build test
.DEFAULT_GOAL := build

dep:
	dep ensure -update

build:
	go build -v -o chess-game-api ./cmd

test:
	go test -v ./...

deploy:
	gcloud app deploy

docker-build:
	docker build -t chess-game-api .
